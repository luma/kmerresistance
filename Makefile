CFLAGS = -Wall -O3 -std=c99
BINS = kmerresistance
LIBS = pherror.o qseqs.o

.c .o:
	$(CC) $(CFLAGS) -c -o $@ $<

all: $(BINS)

kmerresistance: KmerResistance.c $(LIBS)
	$(CC) $(CFLAGS) -o $@ $< $(LIBS) -lm

clean:
	$(RM) $(BINS) $(LIBS)

pherror.o: pherror.h
qseqs.o: qseqs.h pherror.h
